package com.moka.demo.view.ui;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.moka.demo.R;
import com.moka.demo.databinding.DialogFragmentAddEditItemBinding;
import com.moka.demo.model.DiscountPojo;
import com.moka.demo.model.local.MetaItem;
import com.moka.demo.util.Utility;
import com.moka.demo.view_model.ItemListFragmentViewModel;
import com.moka.demo.view_model.ViewModelFactory;

import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

public class AddItemPopupDialog extends DialogFragment implements View.OnClickListener {
    public static final String ARG_SELECTED_ITEM = "selected_item";
    private static final String ARG_DIALOG_TYPE = "dialog_type";
    public static final int TYPE_ADD_ITEM = 1;
    public static final int TYPE_EDIT_ITEM = 2;
    @Inject
    ViewModelFactory mViewModelFactory;
    private ItemListFragmentViewModel viewModel;
    private DialogFragmentAddEditItemBinding binding;
    private MetaItem selectedItem;
    private int dialogType = TYPE_ADD_ITEM;

    public static AddItemPopupDialog newInstance(MetaItem metaItem, int dialogType) {
        Bundle args = new Bundle();
        args.putInt(ARG_DIALOG_TYPE, dialogType);
        args.putParcelable(ARG_SELECTED_ITEM, metaItem);
        AddItemPopupDialog fragment = new AddItemPopupDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_fragment_add_edit_item, container, false);
        viewModel = ViewModelProviders.of(getActivity(), mViewModelFactory).get(ItemListFragmentViewModel.class);
        binding.btnCancel.setOnClickListener(this);
        binding.btnSave.setOnClickListener(this);
        binding.btnMinus.setOnClickListener(this);
        binding.btnPlus.setOnClickListener(this);
        if (getArguments() != null) {
            selectedItem = getArguments().getParcelable(ARG_SELECTED_ITEM);
            dialogType = getArguments().getInt(ARG_DIALOG_TYPE, TYPE_ADD_ITEM);
            if (selectedItem != null) {
                String itemName = selectedItem.getTitle().length() > 8 ? selectedItem.getTitle().substring(0, 8) : selectedItem.getTitle();
                binding.tvItemWithPrice.setText(String.format(Locale.getDefault(), "%s - $ %.2f", itemName, selectedItem.getPrice()));
                if (dialogType == TYPE_EDIT_ITEM) {
                    binding.editQuantity.setText(String.valueOf(selectedItem.getQuantity()));
                }
                addDiscountButtons();
            }
        }
        setCancelable(false);
        return binding.getRoot();
    }

    private void addDiscountButtons() {
        List<DiscountPojo> discountPojoList = Utility.getAllDiscounts(getContext().getAssets());
        View.OnClickListener switchOnClickListener = v -> {
            CompoundButton switchBtn = (CompoundButton) v;
            if (switchBtn.isChecked()) {
                for (int i = 0; i < binding.layoutDiscountSwitch.getChildCount(); i++) {
                    CompoundButton btn = (CompoundButton) binding.layoutDiscountSwitch.getChildAt(i);
                    if (btn != switchBtn && btn.isChecked()) {
                        btn.setChecked(false);
                    }
                }
            }
        };
        for (int i = 0; i < binding.layoutDiscountSwitch.getChildCount(); i++) {
            CompoundButton btn = (CompoundButton) binding.layoutDiscountSwitch.getChildAt(i);
            btn.setText(String.format(Locale.getDefault(), "%s (%.1f%%)", discountPojoList.get(i).getName(), discountPojoList.get(i).getValue()));
            btn.setTag(discountPojoList.get(i));
            if (dialogType == TYPE_EDIT_ITEM && selectedItem.getDiscount() != null && selectedItem.getDiscount().equals(discountPojoList.get(i))) {
                btn.setChecked(true);
            }
            if (dialogType == TYPE_EDIT_ITEM)
                btn.setEnabled(false);
            btn.setOnClickListener(switchOnClickListener);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_cancel:
                dismiss();
                break;
            case R.id.btn_save:
                if (selectedItem != null) {
                    int quantity = Integer.valueOf(binding.editQuantity.getText().toString());
                    if (quantity > 0 && quantity <= 1000) {
                        DiscountPojo discount = getSelectedDiscount();
                        MetaItem newItem = selectedItem.clone();
                        newItem.setQuantity(quantity);
                        newItem.setDiscount(discount);
                        viewModel.onAddOrEditCart(newItem, dialogType == TYPE_EDIT_ITEM);
                        Toast.makeText(getContext(), "Item added to the cart", Toast.LENGTH_SHORT).show();
                        dismiss();
                    } else {
                        Toast.makeText(getContext(), "Valid quantity range 1 - 1000", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case R.id.btn_plus:
                int value = Integer.valueOf(binding.editQuantity.getText().toString());
                if (value < 1000)
                    binding.editQuantity.setText(String.valueOf(value + 1));
                break;
            case R.id.btn_minus:
                value = Integer.valueOf(binding.editQuantity.getText().toString());
                if (value > 0)
                    binding.editQuantity.setText(String.valueOf(value - 1));
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        WindowManager.LayoutParams params = getDialog().getWindow().getAttributes();
        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        params.height = getResources().getDimensionPixelSize(R.dimen.popup_height);
        getDialog().getWindow().setAttributes(params);
    }

    private DiscountPojo getSelectedDiscount() {
        for (int i = 0; i < binding.layoutDiscountSwitch.getChildCount(); i++) {
            CompoundButton btn = (CompoundButton) binding.layoutDiscountSwitch.getChildAt(i);
            if (btn.isChecked()) {
                DiscountPojo discountPojo = (DiscountPojo) btn.getTag();
                if (discountPojo != null) {
                    return discountPojo;
                }
            }
        }
        DiscountPojo discountPojo = new DiscountPojo();
        discountPojo.setValue(0);
        return discountPojo;
    }
}
