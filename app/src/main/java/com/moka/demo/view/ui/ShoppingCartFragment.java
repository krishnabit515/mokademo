package com.moka.demo.view.ui;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.moka.demo.App;
import com.moka.demo.R;
import com.moka.demo.databinding.FragmentShoppingCartBinding;
import com.moka.demo.model.local.MetaItem;
import com.moka.demo.view.ItemClickListener;
import com.moka.demo.view.adapter.ShoppingCartAdapter;
import com.moka.demo.view_model.ItemListFragmentViewModel;
import com.moka.demo.view_model.ViewModelFactory;

import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

public class ShoppingCartFragment extends Fragment implements View.OnClickListener, ItemClickListener {
    @Inject
    ViewModelFactory mViewModelFactory;
    private FragmentShoppingCartBinding binding;
    private ItemListFragmentViewModel viewModel;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_shopping_cart, container, false);
        setupToolbar();
        App.getApp().getViewModelComponent().inject(this);
        viewModel = ViewModelProviders.of(getActivity(), mViewModelFactory).get(ItemListFragmentViewModel.class);
        viewModel.getSelectedItemsData().observe(this, this::refreshCart);
        viewModel.getTotalPriceAndDiscountData().observe(this, subTotalAndDiscount -> {
            binding.btnCharge.setText(String.format(Locale.getDefault(), "Charge $ %.2f", subTotalAndDiscount.get(0) - subTotalAndDiscount.get(1)));
            if (binding.recyclerView.getAdapter() != null) {
                ((ShoppingCartAdapter) binding.recyclerView.getAdapter()).updateTotal(subTotalAndDiscount.get(0), subTotalAndDiscount.get(1));
            }
        });
        binding.btnClear.setOnClickListener(this);
        binding.btnCharge.setOnClickListener(this);
        return binding.getRoot();
    }

    private void refreshCart(List<MetaItem> itemList) {
        if (binding.recyclerView.getAdapter() == null) {
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
            ShoppingCartAdapter adapter = new ShoppingCartAdapter(itemList);
            adapter.setItemClickListener(this);
            binding.recyclerView.setHasFixedSize(true);
            binding.recyclerView.setLayoutManager(linearLayoutManager);
            binding.recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
            binding.recyclerView.setAdapter(adapter);
        } else {
            binding.recyclerView.getAdapter().notifyDataSetChanged();
        }
        if (itemList.size() == 0) {
            binding.tvCartEmpty.setVisibility(View.VISIBLE);
            binding.btnCharge.setEnabled(false);
            binding.btnClear.setEnabled(false);
        } else {
            binding.tvCartEmpty.setVisibility(View.GONE);
            binding.btnCharge.setEnabled(true);
            binding.btnClear.setEnabled(true);
        }
    }

    private void setupToolbar() {
        if (getActivity() == null || getResources().getBoolean(R.bool.twoPaneMode)) return;
        ((AppCompatActivity) getActivity()).setSupportActionBar(binding.toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        binding.toolbar.setNavigationOnClickListener(v -> getActivity().onBackPressed());
    }

    @Override
    public void onClick(View v) {
        if (v == binding.btnClear) {
            viewModel.onClearCartClicked();
        } else if (v == binding.btnCharge) {
            Toast.makeText(getContext(), "Button charge clicked", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onItemClick(MetaItem metaItem) {
        AddItemPopupDialog.newInstance(metaItem, AddItemPopupDialog.TYPE_EDIT_ITEM).show(getFragmentManager(), "popup_dialog");
    }
}
