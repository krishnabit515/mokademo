package com.moka.demo.view.ui;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.moka.demo.App;
import com.moka.demo.R;
import com.moka.demo.databinding.FragmentItemListBinding;
import com.moka.demo.model.local.MetaItem;
import com.moka.demo.view.ItemClickListener;
import com.moka.demo.view.adapter.ItemRecyclerViewAdapter;
import com.moka.demo.view_model.ItemListFragmentViewModel;
import com.moka.demo.view_model.ViewModelFactory;

import java.util.List;

import javax.inject.Inject;

public class ItemListFragment extends Fragment implements ItemClickListener, View.OnClickListener {
    @Inject
    ViewModelFactory mViewModelFactory;
    private FragmentItemListBinding binding;
    private ItemListFragmentViewModel viewModel;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_item_list, container, false);
        setupToolbar();
        binding.btnGoToCart.setOnClickListener(this);
        App.getApp().getViewModelComponent().inject(this);
        viewModel = ViewModelProviders.of(getActivity(), mViewModelFactory).get(ItemListFragmentViewModel.class);
        viewModel.getAllItems().observe(this, this::setupRecyclerView);
        if (getResources().getBoolean(R.bool.twoPaneMode)) {
            binding.btnGoToCart.setVisibility(View.GONE);
        }
        return binding.getRoot();
    }

    private void setupRecyclerView(List<MetaItem> metaItemList) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        ItemRecyclerViewAdapter adapter = new ItemRecyclerViewAdapter(metaItemList);
        adapter.setItemClickListener(this);
        binding.recyclerView.setHasFixedSize(true);
        binding.recyclerView.setLayoutManager(linearLayoutManager);
        binding.recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
        binding.recyclerView.setAdapter(adapter);
    }

    private void setupToolbar() {
        if (getActivity() == null) return;
        ((AppCompatActivity) getActivity()).setSupportActionBar(binding.toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        binding.toolbar.setNavigationOnClickListener(v -> getActivity().onBackPressed());
    }

    @Override
    public void onItemClick(MetaItem discountPojo) {
        AddItemPopupDialog.newInstance(discountPojo, AddItemPopupDialog.TYPE_ADD_ITEM).show(getFragmentManager(), "popup_dialog");
    }

    @Override
    public void onClick(View v) {
        if (v == binding.btnGoToCart) {
            if (getActivity() != null)
                ((MainActivity) getActivity()).showShoppingCart();
        }
    }
}
