package com.moka.demo.view;

import com.moka.demo.model.local.MetaItem;

public interface ItemClickListener {
    void onItemClick(MetaItem metaItem);
}
