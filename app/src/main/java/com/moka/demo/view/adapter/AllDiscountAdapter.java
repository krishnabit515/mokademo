package com.moka.demo.view.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.moka.demo.R;
import com.moka.demo.model.DiscountPojo;

import java.util.List;
import java.util.Locale;

public class AllDiscountAdapter extends RecyclerView.Adapter<AllDiscountAdapter.AllDiscountViewHolder> {
    private List<DiscountPojo> allDiscounts;
    private DiscountClickListener discountClickListener;

    public AllDiscountAdapter(List<DiscountPojo> allDiscounts) {
        this.allDiscounts = allDiscounts;
    }

    public void setDiscountClickListener(DiscountClickListener discountClickListener) {
        this.discountClickListener = discountClickListener;
    }

    @Override
    public AllDiscountViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_all_discount, viewGroup, false);
        return new AllDiscountViewHolder(view, discountClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull AllDiscountViewHolder viewHolder, int i) {
        viewHolder.bind(allDiscounts.get(i));
    }

    @Override
    public int getItemCount() {
        if (allDiscounts != null)
            return allDiscounts.size();
        return 0;
    }

    public static class AllDiscountViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tvDiscountName;
        private TextView tvDiscount;
        private DiscountClickListener discountClickListener;

        public AllDiscountViewHolder(@NonNull View itemView, DiscountClickListener discountClickListener) {
            super(itemView);
            this.discountClickListener = discountClickListener;
            tvDiscountName = itemView.findViewById(R.id.tv_discount_name);
            tvDiscount = itemView.findViewById(R.id.tv_discount);
        }

        public void bind(DiscountPojo discountPojo) {
            tvDiscountName.setText(discountPojo.getName());
            tvDiscount.setText(String.format(Locale.getDefault(), "%.2f %%", discountPojo.getValue()));
            itemView.setTag(discountPojo);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (discountClickListener != null)
                discountClickListener.onClick((DiscountPojo) v.getTag());
        }
    }

    public interface DiscountClickListener {
        void onClick(DiscountPojo discountPojo);
    }
}
