package com.moka.demo.view.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.moka.demo.R;
import com.moka.demo.model.local.MetaItem;
import com.moka.demo.view.ItemClickListener;

import java.util.List;
import java.util.Locale;

public class ShoppingCartAdapter extends RecyclerView.Adapter {
    private final int TYPE_ITEM = 1;
    private final int TYPE_TOTAL = 2;
    private List<MetaItem> allItemsList;
    private double totalDiscount;
    private double subTotalPrice;
    private ItemClickListener itemClickListener;

    public ShoppingCartAdapter(List<MetaItem> allItemsList) {
        this.allItemsList = allItemsList;
    }

    @Override
    public int getItemViewType(int position) {
        if (position < allItemsList.size())
            return TYPE_ITEM;
        else return TYPE_TOTAL;
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        if (i == TYPE_ITEM) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_shopping_cart, viewGroup, false);
            return new AllItemsViewHolder(view, itemClickListener);
        } else {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_shopping_cart_footer, viewGroup, false);
            return new SubTotalViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        if (viewHolder.getItemViewType() == TYPE_ITEM)
            ((AllItemsViewHolder) viewHolder).bind(allItemsList.get(i));
        else
            ((SubTotalViewHolder) viewHolder).bind(subTotalPrice, totalDiscount);
    }

    @Override
    public int getItemCount() {
        if (allItemsList != null && allItemsList.size() > 0) {
            return allItemsList.size() + 1;
        }
        return 0;
    }

    public void updateTotal(double subTotal, double totalDiscount) {
        this.subTotalPrice = subTotal;
        this.totalDiscount = totalDiscount;
        notifyItemChanged(getItemCount() - 1);
    }

    public static class AllItemsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tvItemName;
        private TextView tvQuantity;
        private TextView tvTotalPrice;
        private ItemClickListener itemClickListener;

        public AllItemsViewHolder(@NonNull View itemView, ItemClickListener itemClickListener) {
            super(itemView);
            this.itemClickListener = itemClickListener;
            tvItemName = itemView.findViewById(R.id.tv_item_name);
            tvQuantity = itemView.findViewById(R.id.tv_quantity);
            tvTotalPrice = itemView.findViewById(R.id.tv_total_price);
        }

        public void bind(MetaItem metaItem) {
            tvItemName.setText(metaItem.getTitle());
            tvQuantity.setText(String.format("x%s", String.valueOf(metaItem.getQuantity())));
            tvTotalPrice.setText(String.format(Locale.getDefault(), "$ %.2f", metaItem.getQuantity() * metaItem.getPrice()));
            itemView.setTag(metaItem);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (itemClickListener != null)
                itemClickListener.onItemClick((MetaItem) v.getTag());
        }
    }

    public static class SubTotalViewHolder extends RecyclerView.ViewHolder {
        private TextView tvTotalPrice;
        private TextView tvTotalDiscount;

        public SubTotalViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTotalPrice = itemView.findViewById(R.id.tv_total_price);
            tvTotalDiscount = itemView.findViewById(R.id.tv_total_discount);
        }

        public void bind(double subTotalPrice, double totaDiscount) {
            tvTotalPrice.setText(String.format(Locale.getDefault(), "$ %.2f", subTotalPrice));
            tvTotalDiscount.setText(String.format(Locale.getDefault(), "($ %.2f)", totaDiscount));
        }
    }
}
