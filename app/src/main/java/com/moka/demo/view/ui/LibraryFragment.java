package com.moka.demo.view.ui;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.moka.demo.R;
import com.moka.demo.databinding.FragmentLibraryBinding;

public class LibraryFragment extends Fragment implements View.OnClickListener {
    private FragmentLibraryBinding binding;
    private Communicator communicator;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_library, container, false);
        binding.btnAllItems.setOnClickListener(this);
        binding.btnDiscount.setOnClickListener(this);
        return binding.getRoot();
    }

    @Override
    public void onClick(View v) {
        if (v == binding.btnAllItems) {
            if (communicator != null)
                communicator.onAllItemClick();
        } else if (v == binding.btnDiscount) {
            if (communicator != null)
                communicator.onAllDiscountClick();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        communicator = (Communicator) context;
    }

    public interface Communicator {
        void onAllDiscountClick();

        void onAllItemClick();
    }
}
