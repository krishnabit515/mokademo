package com.moka.demo.view.ui;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.moka.demo.App;
import com.moka.demo.R;
import com.moka.demo.databinding.FragmentDiscountListBinding;
import com.moka.demo.model.DiscountPojo;
import com.moka.demo.view.adapter.AllDiscountAdapter;
import com.moka.demo.view_model.DiscountListFragmentViewModel;
import com.moka.demo.view_model.ViewModelFactory;

import java.util.List;

import javax.inject.Inject;

public class DiscountListFragment extends Fragment implements AllDiscountAdapter.DiscountClickListener {
    @Inject
    ViewModelFactory mViewModelFactory;
    private FragmentDiscountListBinding binding;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_discount_list, container, false);
        setupToolbar();
        App.getApp().getViewModelComponent().inject(this);
        DiscountListFragmentViewModel viewModel = ViewModelProviders.of(this, mViewModelFactory).get(DiscountListFragmentViewModel.class);
        viewModel.getDiscountsListData().observe(this, this::setupRecyclerView);
        viewModel.fetchAllDiscounts();
        return binding.getRoot();
    }

    private void setupRecyclerView(List<DiscountPojo> discountPojos) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        AllDiscountAdapter adapter = new AllDiscountAdapter(discountPojos);
        adapter.setDiscountClickListener(this);
        binding.recyclerView.setHasFixedSize(true);
        binding.recyclerView.setLayoutManager(linearLayoutManager);
        binding.recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
        binding.recyclerView.setAdapter(adapter);
    }

    private void setupToolbar() {
        if (getActivity() == null) return;
        ((AppCompatActivity) getActivity()).setSupportActionBar(binding.toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        binding.toolbar.setNavigationOnClickListener(v -> getActivity().onBackPressed());
    }

    @Override
    public void onClick(DiscountPojo discountPojo) {

    }
}
