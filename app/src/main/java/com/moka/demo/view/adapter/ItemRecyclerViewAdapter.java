package com.moka.demo.view.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.moka.demo.R;
import com.moka.demo.model.local.MetaItem;
import com.moka.demo.view.ItemClickListener;

import java.util.List;
import java.util.Locale;
import java.util.Random;

public class ItemRecyclerViewAdapter extends RecyclerView.Adapter<ItemRecyclerViewAdapter.AllDiscountViewHolder> {
    private List<MetaItem> metaItemList;
    private ItemClickListener itemClickListener;

    public ItemRecyclerViewAdapter(List<MetaItem> metaItemList) {
        this.metaItemList = metaItemList;
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public AllDiscountViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_all_items, viewGroup, false);
        return new AllDiscountViewHolder(view, itemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull AllDiscountViewHolder viewHolder, int i) {
        viewHolder.bind(metaItemList.get(i));
    }

    @Override
    public int getItemCount() {
        if (metaItemList != null)
            return metaItemList.size();
        return 0;
    }

    public static class AllDiscountViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tvItemName;
        private TextView tvPrice;
        private ImageView ivThumbnail;
        private ItemClickListener itemClickListener;

        public AllDiscountViewHolder(@NonNull View itemView, ItemClickListener itemClickListener) {
            super(itemView);
            this.itemClickListener = itemClickListener;
            tvItemName = itemView.findViewById(R.id.tv_item_name);
            ivThumbnail = itemView.findViewById(R.id.iv_thumbnail);
            tvPrice = itemView.findViewById(R.id.tv_price);
        }

        public void bind(MetaItem metaItem) {
            tvItemName.setText(metaItem.getTitle());
            int price = metaItem.getPrice() == 0 ? calculatePrice(metaItem.getId()) : (int) metaItem.getPrice();
            metaItem.setPrice(price);
            tvPrice.setText(String.format(Locale.getDefault(), "$ %d", price));
            Glide.with(itemView).load(metaItem.getThumbnailUrl()).into(ivThumbnail);
            itemView.setTag(metaItem);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (itemClickListener != null)
                itemClickListener.onItemClick((MetaItem) v.getTag());
        }

        private int calculatePrice(int id) {
            Random random = new Random(565453);
            return id * random.nextInt(99 - 10) + 10;
        }
    }
}
