package com.moka.demo.view.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.moka.demo.R;

import java.util.List;

public class MainActivity extends AppCompatActivity implements LibraryFragment.Communicator {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState != null) {
            List<Fragment> fragmentList = getSupportFragmentManager().getFragments();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            for (Fragment fragment : fragmentList) {
                if (fragment != null)
                    transaction.remove(fragment);
            }
            transaction.commit();
        }
        if (getResources().getBoolean(R.bool.twoPaneMode)) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_list_container, new LibraryFragment())
                    .replace(R.id.fragment_shopping_cart_container, new ShoppingCartFragment())
                    .commit();
        } else {
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new LibraryFragment())
                    .commit();
        }
    }

    @Override
    public void onAllDiscountClick() {
        if (getResources().getBoolean(R.bool.twoPaneMode)) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_list_container, new DiscountListFragment())
                    .addToBackStack(null).commit();
        } else {
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new DiscountListFragment())
                    .addToBackStack(null).commit();
        }
    }

    @Override
    public void onAllItemClick() {
        if (getResources().getBoolean(R.bool.twoPaneMode)) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_list_container, new ItemListFragment())
                    .addToBackStack(null).commit();
        } else {
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new ItemListFragment())
                    .addToBackStack(null).commit();
        }
    }

    public void showShoppingCart() {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, new ShoppingCartFragment())
                .addToBackStack(null).commit();
    }
}
