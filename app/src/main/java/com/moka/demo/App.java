package com.moka.demo;

import android.app.Application;

import com.moka.demo.di.DaggerDataComponent;
import com.moka.demo.di.DataComponent;
import com.moka.demo.di.DataModule;
import com.moka.demo.di.ViewModelComponent;
import com.moka.demo.di.ViewModelModule;

public class App extends Application {
    private static App app;
    private ViewModelComponent viewModelComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
        initComponents();
    }

    private void initComponents() {
        DataComponent dataComponent = DaggerDataComponent.builder()
                .dataModule(new DataModule(this))
                .build();
        viewModelComponent = dataComponent.newViewModuleComponent(new ViewModelModule());
        dataComponent.inject(this);
    }

    public static App getApp() {
        return app;
    }

    public ViewModelComponent getViewModelComponent() {
        return viewModelComponent;
    }
}
