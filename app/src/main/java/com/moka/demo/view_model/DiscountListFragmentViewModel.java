package com.moka.demo.view_model;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.content.res.AssetManager;

import com.moka.demo.model.DiscountPojo;
import com.moka.demo.util.Utility;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class DiscountListFragmentViewModel extends ViewModel {
    private AssetManager assetManager;
    private MutableLiveData<List<DiscountPojo>> discountsListData = new MutableLiveData<>();
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Inject
    public DiscountListFragmentViewModel(AssetManager assetManager) {
        this.assetManager = assetManager;
    }

    public void fetchAllDiscounts() {
        Single<List<DiscountPojo>> discountListObservable = Single.fromCallable(() -> Utility.getAllDiscounts(assetManager));
        compositeDisposable.add(
                discountListObservable.subscribeOn(Schedulers.io())
                        .subscribe(discountPojos -> discountsListData.postValue(discountPojos)));
    }

    public MutableLiveData<List<DiscountPojo>> getDiscountsListData() {
        return discountsListData;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        compositeDisposable.dispose();
    }
}
