package com.moka.demo.view_model;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.util.Log;

import com.moka.demo.model.local.MetaItem;
import com.moka.demo.model.repository.Repository;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class ItemListFragmentViewModel extends ViewModel {
    private static final String TAG = "ItemListFragmentViewMod";
    private Repository repository;
    private MutableLiveData<List<MetaItem>> selectedItemsData = new MutableLiveData<>();
    private MutableLiveData<List<Double>> totalPriceAndDiscountData = new MutableLiveData<>();

    @Inject
    public ItemListFragmentViewModel(Repository repository) {
        this.repository = repository;
    }

    public LiveData<List<MetaItem>> getAllItems() {
        return repository.getAllItems();
    }

    public void onAddOrEditCart(MetaItem item, boolean edit) {
        List<MetaItem> selectedItemsList = selectedItemsData.getValue();
        if (selectedItemsList == null) selectedItemsList = new ArrayList<>();

        //check if this item with same discount already exist in the list
        int alreadyAddedItemIndex = indexOfItem(selectedItemsList, item);
        if (alreadyAddedItemIndex == -1) {
            item.setQuantity(item.getQuantity());
            selectedItemsList.add(item);
            Log.d(TAG, "new item added " + item);
        } else {
            //item already exists, increase the quantity
            MetaItem alreadyAddedItem = selectedItemsList.get(alreadyAddedItemIndex);
            int newQuantity = edit ? item.getQuantity() : alreadyAddedItem.getQuantity() + item.getQuantity();
            alreadyAddedItem.setQuantity(newQuantity);
            Log.d(TAG, "item updated " + alreadyAddedItem);
        }
        selectedItemsData.setValue(selectedItemsList);
        calculateSubTotalAndDiscount();
    }

    private int indexOfItem(List<MetaItem> selectedItemsList, MetaItem itemToAdd) {
        MetaItem metaItem;
        for (int i = 0; i < selectedItemsList.size(); i++) {
            metaItem = selectedItemsList.get(i);
            if (metaItem.getId() == itemToAdd.getId() && (metaItem.getDiscount().equals(itemToAdd.getDiscount()))) {
                return i;
            }
        }
        return -1;
    }

    private void calculateSubTotalAndDiscount() {
        List<Double> totalPriceAndDiscount = totalPriceAndDiscountData.getValue();
        if (totalPriceAndDiscount == null) {
            totalPriceAndDiscount = new ArrayList<>();
            totalPriceAndDiscount.add((double) 0);
            totalPriceAndDiscount.add((double) 0);
        } else {
            totalPriceAndDiscount.set(0, (double) 0);
            totalPriceAndDiscount.set(1, (double) 0);
        }

        for (MetaItem item : selectedItemsData.getValue()) {
            totalPriceAndDiscount.set(0, totalPriceAndDiscount.get(0) + item.getPrice() * item.getQuantity());
            if (item.getDiscount().getValue() > 0) {
                totalPriceAndDiscount.set(1, totalPriceAndDiscount.get(1) + item.getPrice() * item.getQuantity() * (item.getDiscount().getValue() / 100.0));
            }
        }
        totalPriceAndDiscountData.setValue(totalPriceAndDiscount);
    }

    public MutableLiveData<List<MetaItem>> getSelectedItemsData() {
        return selectedItemsData;
    }

    public MutableLiveData<List<Double>> getTotalPriceAndDiscountData() {
        return totalPriceAndDiscountData;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        repository.cancelSubscriptions();
    }

    public void onClearCartClicked() {
        List<Double> subTotals = totalPriceAndDiscountData.getValue();
        if (subTotals != null) {
            subTotals.set(0, (double) 0);
            subTotals.set(1, (double) 0);
            totalPriceAndDiscountData.setValue(subTotals);
        }
        List<MetaItem> selectedItems = selectedItemsData.getValue();
        if (selectedItems != null) {
            selectedItems.clear();
            selectedItemsData.setValue(selectedItems);
        }
    }
}
