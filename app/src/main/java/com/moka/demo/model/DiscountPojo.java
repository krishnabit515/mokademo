package com.moka.demo.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class DiscountPojo implements Parcelable {
    @SerializedName("name")
    private String name;
    @SerializedName("value")
    private double value;

    public DiscountPojo() {
    }

    protected DiscountPojo(Parcel in) {
        name = in.readString();
        value = in.readDouble();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeDouble(value);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DiscountPojo> CREATOR = new Creator<DiscountPojo>() {
        @Override
        public DiscountPojo createFromParcel(Parcel in) {
            return new DiscountPojo(in);
        }

        @Override
        public DiscountPojo[] newArray(int size) {
            return new DiscountPojo[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DiscountPojo that = (DiscountPojo) o;
        return Double.compare(that.value, value) == 0 &&
                (name == that.name || name != null && name.equals(that.name));
    }

    @Override
    public int hashCode() {
        double result = value;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return (int) result;
    }
}
