package com.moka.demo.model.local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface ItemDao {
    @Query("SELECT * FROM MetaItem")
    List<MetaItem> getAll();

    @Query("SELECT COUNT(*) FROM MetaItem")
    int getCount();

    @Insert
    void insertAll(List<MetaItem> itemList);

    @Delete
    void delete(MetaItem item);
}
