package com.moka.demo.model.remote;

import com.moka.demo.model.Item;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;

public interface ApiService {
    String BASE_URL = "https://jsonplaceholder.typicode.com/";

    @GET("photos/")
    Single<List<Item>> getAllItems();
}
