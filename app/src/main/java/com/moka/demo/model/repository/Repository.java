package com.moka.demo.model.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.moka.demo.model.local.AppDatabase;
import com.moka.demo.model.local.MetaItem;
import com.moka.demo.model.remote.ApiService;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class Repository {
    private ApiService apiService;
    private AppDatabase appDatabase;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Inject
    public Repository(ApiService apiService, AppDatabase appDatabase) {
        this.apiService = apiService;
        this.appDatabase = appDatabase;
    }

    public LiveData<List<MetaItem>> getAllItems() {
        MutableLiveData<List<MetaItem>> itemsData = new MutableLiveData<>();
        Observable<List<MetaItem>> observable = Observable.fromCallable(() -> appDatabase.itemDao().getAll());
        compositeDisposable.add(
                observable.subscribeOn(Schedulers.io())
                        .subscribe(allItems -> {
                            if (allItems == null || allItems.size() == 0) {
                                compositeDisposable.add(
                                        apiService.getAllItems()
                                                .map(MetaItem::from)
                                                .subscribe(items -> {
                                                    itemsData.postValue(items);
                                                    appDatabase.itemDao().insertAll(items);
                                                }, err -> {
                                                }));
                            } else {
                                itemsData.postValue(allItems);
                            }
                        }, Throwable::printStackTrace));
        return itemsData;
    }

    public void cancelSubscriptions() {
        compositeDisposable.dispose();
    }
}
