package com.moka.demo.model.local;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.os.Parcel;
import android.os.Parcelable;

import com.moka.demo.model.DiscountPojo;
import com.moka.demo.model.Item;

import java.util.ArrayList;
import java.util.List;

@Entity(primaryKeys = {"album_id", "id"})
public class MetaItem implements Parcelable {
    @ColumnInfo(name = "album_id")
    private int albumId;

    private int id;

    private String title;

    private String url;

    @ColumnInfo(name = "thumbnail_url")
    private String thumbnailUrl;

    @Ignore
    private int quantity;

    @Ignore
    private double price;

    @Ignore
    private DiscountPojo discount;

    public MetaItem() {
    }

    protected MetaItem(Parcel in) {
        albumId = in.readInt();
        id = in.readInt();
        title = in.readString();
        url = in.readString();
        thumbnailUrl = in.readString();
        quantity = in.readInt();
        price = in.readDouble();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(albumId);
        dest.writeInt(id);
        dest.writeString(title);
        dest.writeString(url);
        dest.writeString(thumbnailUrl);
        dest.writeInt(quantity);
        dest.writeDouble(price);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MetaItem> CREATOR = new Creator<MetaItem>() {
        @Override
        public MetaItem createFromParcel(Parcel in) {
            return new MetaItem(in);
        }

        @Override
        public MetaItem[] newArray(int size) {
            return new MetaItem[size];
        }
    };

    public static List<MetaItem> from(List<Item> itemsList) {
        List<MetaItem> metaItemList = null;
        if (itemsList != null) {
            metaItemList = new ArrayList<>();
            for (Item item : itemsList) {
                MetaItem metaItem = new MetaItem();
                metaItem.albumId = item.getAlbumId();
                metaItem.id = item.getId();
                metaItem.title = item.getTitle();
                metaItem.thumbnailUrl = item.getThumbnailUrl();
                metaItem.url = item.getUrl();
                metaItemList.add(metaItem);
            }
        }
        return metaItemList;
    }

    public int getAlbumId() {
        return albumId;
    }

    public void setAlbumId(int albumId) {
        this.albumId = albumId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public DiscountPojo getDiscount() {
        return discount;
    }

    public void setDiscount(DiscountPojo discount) {
        this.discount = discount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MetaItem item = (MetaItem) o;
        return id == item.id &&
                (discount == item.discount) || (discount != null && discount.equals(item.discount));
    }

    @Override
    public int hashCode() {
        double result = id;
        result = 31 * result + (discount != null ? discount.hashCode() : 0);
        return (int) result;
    }

    public MetaItem clone() {
        MetaItem copy = new MetaItem();
        copy.setId(getId());
        copy.setAlbumId(getAlbumId());
        copy.setTitle(getTitle());
        copy.setQuantity(getQuantity());
        copy.setPrice(getPrice());
        copy.setDiscount(getDiscount());
        copy.setThumbnailUrl(getThumbnailUrl());
        return copy;
    }

    @Override
    public String toString() {
        return "MetaItem{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", quantity=" + quantity +
                ", price=" + price +
                ", discount=" + discount +
                '}';
    }
}
