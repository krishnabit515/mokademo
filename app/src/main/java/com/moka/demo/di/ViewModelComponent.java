package com.moka.demo.di;

import com.moka.demo.view.ui.DiscountListFragment;
import com.moka.demo.view.ui.ItemListFragment;
import com.moka.demo.view.ui.ShoppingCartFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {ViewModelModule.class})
public interface ViewModelComponent {
    void inject(DiscountListFragment discountListFragment);

    void inject(ItemListFragment itemListFragment);

    void inject(ShoppingCartFragment shoppingCartFragment);
}
