package com.moka.demo.di;

import com.moka.demo.App;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = DataModule.class)
public interface DataComponent {
    ViewModelComponent newViewModuleComponent(ViewModelModule viewModelModule);

    void inject(App app);
}
