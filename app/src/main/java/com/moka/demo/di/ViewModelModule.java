package com.moka.demo.di;

import android.arch.lifecycle.ViewModel;
import android.content.res.AssetManager;

import com.moka.demo.model.repository.Repository;
import com.moka.demo.view_model.DiscountListFragmentViewModel;
import com.moka.demo.view_model.ItemListFragmentViewModel;
import com.moka.demo.view_model.ViewModelFactory;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Map;

import javax.inject.Provider;

import dagger.MapKey;
import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoMap;

@Module
public class ViewModelModule {

    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.RUNTIME)
    @MapKey
    @interface ViewModelKey {
        Class<? extends ViewModel> value();
    }

    @Provides
    ViewModelFactory viewModelFactory(Map<Class<? extends ViewModel>, Provider<ViewModel>> providerMap) {
        return new ViewModelFactory(providerMap);
    }

    @Provides
    @IntoMap
    @ViewModelKey(DiscountListFragmentViewModel.class)
    ViewModel discountListFragmentViewModel(AssetManager assetManager) {
        return new DiscountListFragmentViewModel(assetManager);
    }

    @Provides
    @IntoMap
    @ViewModelKey(ItemListFragmentViewModel.class)
    ViewModel itemListFragmentViewModel(Repository repository) {
        return new ItemListFragmentViewModel(repository);
    }
}
