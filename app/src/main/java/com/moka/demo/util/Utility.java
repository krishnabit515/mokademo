package com.moka.demo.util;

import android.content.res.AssetManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.moka.demo.model.DiscountPojo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class Utility {

    public static List<DiscountPojo> getAllDiscounts(AssetManager assetManager) {
        List<DiscountPojo> allDiscountsList = null;
        if (assetManager == null) return allDiscountsList;
        try {
            StringBuilder sb = new StringBuilder();
            InputStream is = assetManager.open("all_discount.json");
            BufferedReader br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            String str;
            while ((str = br.readLine()) != null) {
                sb.append(str);
            }
            br.close();
            Gson gson = new Gson();
            Type type = new TypeToken<ArrayList<DiscountPojo>>() {
            }.getType();
            allDiscountsList = gson.fromJson(sb.toString(), type);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return allDiscountsList;
    }
}
